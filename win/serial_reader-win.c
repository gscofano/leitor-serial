#include <winioctl.h>
#include <stdio.h>

#define BUFFER_SIZE 32

int main(int argc, char *argv[])
{
	printf("\nComecando...\n");

	int resultado;				//Variável utilizada para avaliar resultados de chamadas de funções
	COMMTIMEOUTS timeouts = { 0 };
	DCB parametros_comm = {0};
	HANDLE comm_handle;			//Handle utilizado para acessar a porta serial
	DWORD mascara_evento;
	char  char_lido; 			// Temperory Character
	char  buffer_serial[BUFFER_SIZE];		// Buffer Containing Rxed Data
	DWORD n_bytes_lidos;		// Bytes read by ReadFile()
	int i;
	int j;
	int contador_buffer = 0;
	int mask = 0x80; /* 10000000 */

	parametros_comm.DCBlength = sizeof(parametros_comm);

	comm_handle = CreateFile(argv[1], 
		GENERIC_READ,
		0,
		NULL, 
		OPEN_EXISTING,
		0,
		NULL);

	printf("Abrindo a porta serial...\t\t\t");
	if(comm_handle == INVALID_HANDLE_VALUE)
	{
		printf("ERRO\n");
		return -1;
	}
	else
		printf("OK\n");

	printf("Lendo o estado da porta serial...\t\t");
	resultado = GetCommState(comm_handle, &parametros_comm);
	if(resultado == FALSE)
	{	
		printf("ERRO\n");
		return -1;
	}
	else
		printf("OK\n");

	printf("Configurando o estado da porta serial...\t");
	parametros_comm.BaudRate = 1000000;
	parametros_comm.ByteSize = 8;
	parametros_comm.StopBits = ONESTOPBIT;
	parametros_comm.Parity = NOPARITY;
	resultado = SetCommState(comm_handle, &parametros_comm);
	if(resultado == FALSE)
	{	
		printf("ERRO\n");
		return -1;
	}
	else
	{
		printf("OK\n");
	}

	printf("Configurando timeouts...\t\t\t");
	timeouts.ReadIntervalTimeout         = 500; // in milliseconds
	timeouts.ReadTotalTimeoutConstant    = 500; // in milliseconds
	timeouts.ReadTotalTimeoutMultiplier  = 100; // in milliseconds
	timeouts.WriteTotalTimeoutConstant   = 500; // in milliseconds
	timeouts.WriteTotalTimeoutMultiplier = 100; // in milliseconds
	resultado = SetCommTimeouts(comm_handle, &timeouts);
	if(resultado == FALSE)
	{	
		printf("ERRO\n");
		return -1;
	}
	else
		printf("OK\n");

	printf("Limpando buffer de entrada...\t\t\t");
	resultado = PurgeComm(comm_handle, PURGE_RXCLEAR);
	if(resultado == FALSE)
	{	
		printf("ERRO\n");
		return -1;
	}
	else
		printf("OK\n");

	printf("Aplicando máscaras de comunicação...\t\t");
	resultado = SetCommMask(comm_handle, EV_RXCHAR);
	if(resultado == FALSE)
	{	
		printf("ERRO\n");
		return -1;
	}
	else
		printf("OK\n");

	int escape = 123;
	printf("Registrando evento...\t\t\t\t");
	resultado = WaitCommEvent(comm_handle, &mascara_evento, NULL);
	if(resultado == FALSE)
	{	
		printf("ERRO\n");
		return -1;
	}
	else
		printf("OK\n");

	printf("\n\nRecebido:\n");
	printf("1 - ");
	int n_linha = 2;
	do
	{
		resultado = ReadFile(comm_handle, &char_lido, sizeof(char_lido), &n_bytes_lidos, NULL);
		buffer_serial[contador_buffer]=char_lido;
		contador_buffer++;
		if(char_lido=='B')
		{
			for(i=0;i<contador_buffer-1;i++)
				printf("%c", buffer_serial[i]);
			printf("Caractere de parada recebido.\r\n");
			break;
		}
		if(contador_buffer>=BUFFER_SIZE-1)
		{
			for(i=0;i<contador_buffer;i++)
				printf("%c", buffer_serial[i]);
			contador_buffer = 0;
		}

	}while(n_bytes_lidos>0);

	CloseHandle(comm_handle);

	printf("\nTerminando...\n");
} 