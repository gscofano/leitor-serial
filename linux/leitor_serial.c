//TODO: descobrir o porque de TAMANHO_BUFFER maior que 1 dar problema
//		mais velocidade na porta serial

#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

#define TAMANHO_BUFFER 1

int main(int argc, char *argv[])
{
	struct termios config;									//estrutura de configuraçao da porta serial
	unsigned char buffer_entrada[TAMANHO_BUFFER];			//buffer que recebe os dados da porta serial
	int serial_fd = open(argv[1], O_RDONLY | O_NONBLOCK);	//arquivo da porta serial
	int output_fd = open(argv[2], O_WRONLY | O_NONBLOCK);	//os dados da porta serial serao salvos neste arquivo

	if(serial_fd < 0)
	{
		printf("Erro abrindo porta serial.\n");
		return -1;
	}

	if(!isatty(serial_fd))
	{
		printf("O arquivo especificado nao corresponde a uma porta serial.\n");
		return -2;
	}

	if(tcgetattr(serial_fd, &config)<0)
	{
		printf("Erro pegando parametros de porta serial.\n");
		return -3;
	}

	config.c_iflag = (IGNBRK | IGNPAR | !INPCK | !ISTRIP | !INLCR | !IGNCR | !ICRNL);
	config.c_cflag = (CS8 | !CSTOPB | CREAD | !PARENB);
	config.c_lflag = (!ISIG | !ICANON | !ECHO);
	config.c_cc[VMIN] = TAMANHO_BUFFER;
	config.c_cc[VTIME] = 0;

	if(cfsetispeed(&config, B1000000)<0)
	{
		printf("Erro configurando velocidade da porta serial.\n");
		return -4;
	}

	if(tcsetattr(serial_fd, TCSANOW, &config)<0)
	{
		printf("Erro aplicando as configuraçoes da porta serial.\n");
		return -5;
	}

	if(tcflush(serial_fd, TCIFLUSH)<0)
	{
		printf("Erro aplicando as configuraçoes da porta serial.\n");
		return -6;
	}

	printf("Recebendo dados. O pograma finaliza quando o dispositivo remoto enviar a sequencia \"BYE\\n\"\n.");
	printf("Pressione Ctrl+C para abortar.\n");

	do
	{
		if(read(serial_fd, buffer_entrada, TAMANHO_BUFFER) != -1)
		{
			for(int k=0;k<TAMANHO_BUFFER;k++)
				printf("%c", buffer_entrada[k]);
		}
	}while(buffer_entrada[0]!='B');

	close(serial_fd);
	close(output_fd);

}